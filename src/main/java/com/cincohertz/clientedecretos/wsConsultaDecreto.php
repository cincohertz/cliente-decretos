<?xml version="1.0" encoding="ISO-8859-1"?>
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="urn:consultaDecretowsdl" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="urn:consultaDecretowsdl">
<types>
<xsd:schema targetNamespace="urn:consultaDecretowsdl"
>
 <xsd:import namespace="http://schemas.xmlsoap.org/soap/encoding/" />
 <xsd:import namespace="http://schemas.xmlsoap.org/wsdl/" />
</xsd:schema>
</types>
<message name="consultaDecretoRequest">
  <part name="iue" type="xsd:string" />
  <part name="nro_decreto" type="xsd:string" /></message>
<message name="consultaDecretoResponse">
  <part name="texto_decreto" type="xsd:string" /></message>
<portType name="consultaDecretowsdlPortType">
  <operation name="consultaDecreto">
    <documentation>Dada una iue y un nro de decreto, devuelve el texto del mismo</documentation>
    <input message="tns:consultaDecretoRequest"/>
    <output message="tns:consultaDecretoResponse"/>
  </operation>
</portType>
<binding name="consultaDecretowsdlBinding" type="tns:consultaDecretowsdlPortType">
  <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
  <operation name="consultaDecreto">
    <soap:operation soapAction="urn:consultaDecretowsdl#consultaDecreto" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:consultaDecretowsdl" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:consultaDecretowsdl" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
</binding>
<service name="consultaDecretowsdl">
  <port name="consultaDecretowsdlPort" binding="tns:consultaDecretowsdlBinding">
    <soap:address location="http://expedientes.poderjudicial.gub.uy/wsConsultaDecreto.php"/>
  </port>
</service>
</definitions>